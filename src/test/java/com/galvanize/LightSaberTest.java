package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;



public class LightSaberTest {

    LightSaber saber = new LightSaber(29384732);
    LightSaber saber2 = new LightSaber(29384733);
    LightSaber saber3 = new LightSaber(29384734);
    @Test
    public void checkSetChargeChangesChargeValueWithGetCharge(){
        saber.setCharge(100.0f);
        assertEquals(100.0f, saber.getCharge());
    }

    @Test
    public void checkGetChargeChangesChargeValueWithSetCharge(){
        saber.setCharge(100.0f);
        assertEquals(100.0f, saber.getCharge());
    }

    @Test
    public void checkSetColorChangesColorWithGetColor(){
        saber.setColor("blue");
        saber3.setColor("purple");
        assertEquals("blue", saber.getColor());
        assertEquals("green", saber2.getColor());
        assertEquals("purple", saber3.getColor());
    }

    @Test
    public void checkGetColorChangesColorWithSetColor(){
        saber.setColor("blue");
        saber3.setColor("purple");
        assertEquals("blue", saber.getColor());
        assertEquals("green", saber2.getColor());
        assertEquals("purple", saber3.getColor());
    }

    @Test
    public void checkGetJediSerialNumber(){
        assertEquals(29384732, saber.getJediSerialNumber());
        assertEquals(29384733, saber2.getJediSerialNumber());
        assertEquals(29384734, saber3.getJediSerialNumber());
    }

    @Test
    public void checkUseByGetCharge(){
        saber.use(60f);
        saber2.use(120f);
        saber3.use(180f);
        assertEquals(90f, saber.getCharge());
        assertEquals(80f, saber2.getCharge());
        assertEquals(70f, saber3.getCharge());
    }

    @Test
    public void checkGetMinutesRemainingByUse(){
        saber.use(60f);
        saber2.use(120f);
        saber3.use(180f);
        assertEquals(270f, saber.getRemainingMinutes());
        assertEquals(240f, saber2.getRemainingMinutes());
        assertEquals(210f, saber3.getRemainingMinutes());
    }

    @Test
    public void checkRechargeByGettingCharge(){
        saber.use(60f);
        saber2.use(120f);
        saber3.use(180f);
        saber.recharge();
        saber2.recharge();
        saber3.recharge();
        assertEquals(100f, saber.getCharge());
        assertEquals(100f, saber2.getCharge());
        assertEquals(100f, saber3.getCharge());
    }





}
